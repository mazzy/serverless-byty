const JSONUtils = require("../../../helpers/json");

test("return proper url", () => {
  const estate = {
    hash_id: "3279950172",
    seo: {
      category_sub_cb: 8,
      locality: "praha-nusle-horni"
    }
  };
  const url = JSONUtils.buildUrl(estate);
  const expectedUrl =
    "https://www.sreality.cz/detail/prodej/byt/4+kk/praha-nusle-horni/3279950172#img=0&fullscreen=false";

  expect(url).toBe(expectedUrl);
});
