const co = require("co");

const DynamoDB = require("./lib/dynamodb");
const sreality = require("./lib/api");
const sendEmail = require("./lib/email");
const metrics = require("./lib/metrics");

const JSONUtils = require("./helpers/json");
const HashTable = require("./helpers/hashtable");

module.exports.run = co.wrap(function* run(event, context, callback) {
  // count estates for pagination
  const count = yield sreality.countEstates();

  // paginate API
  const itemsPerPage = 20;
  const pages = Math.ceil(count / itemsPerPage);
  const queries = [];

  for (let i = 0; i < pages; i += 1) {
    const page = i + 1;
    queries.push(sreality.getEstates(page));
  }

  const values = yield queries;

  // extract relevant props from the API response
  const estates = []
    .concat(...values)
    .map(JSONUtils.mapEstateProps)
    .filter(JSONUtils.filterLuxuryEstate);

  const params = {
    TableName: process.env.DYNAMODB_TABLE
  };
  const result = yield DynamoDB.scan(params).promise();

  // Put the DynamoDB items to HashTable
  const items = result.Items;
  const htEstates = new HashTable();
  const diffItems = [];

  items.forEach(item => htEstates.put(item.id, item));

  // iterate over the list
  estates.forEach(estate => {
    if (!htEstates.contains(estate.id)) {
      // return the elemen which is not in the database (Hash Table)
      diffItems.push(estate);
    }
  });

  // write metrics
  const data = [
    {
      name: metrics.SEARCH_FETCHED_ITEMS,
      value: estates.length
    },
    {
      name: metrics.SEARCH_NEW_ITEMS,
      value: diffItems.length
    }
  ];

  yield metrics.putMetricData(context.functionName, data);

  const chunk = 25; // 25 is the max item to write in batch
  const batchWrites = [];

  for (let i = 0; i < diffItems.length; i += chunk) {
    const tempArray = diffItems.slice(i, i + chunk);
    // store the new element in the database
    const params = {
      RequestItems: {
        [process.env.DYNAMODB_TABLE]: tempArray.map(item => ({
          PutRequest: {
            Item: JSONUtils.mapItemSchema(item)
          }
        }))
      }
    };
    batchWrites.push(DynamoDB.batchWrite(params).promise());
  }

  yield batchWrites;

  // not send an email if there is no update
  if (diffItems.length > 0) {
    yield sendEmail({ newEstates: diffItems });
  }

  callback(null, { success: true, count: diffItems.length, diffItems });
});
