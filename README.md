# Serverless Byty

[![pipeline status](https://gitlab.com/mazzy/serverless-byty/badges/master/pipeline.svg)](https://gitlab.com/mazzy/serverless-byty/commits/master)

Run an AWS Lambda function based on [Serverless](https://serverless.com/) in Node.js to scrape new real estate properties in Prague and send an alert to the subscribed users via e-mail.

## AWS Services

* Lambda
* DynamoDB
* CloudWatch
* X-Ray

## Roadmap

### 0.1.0

* Fetch new apartments from SReality.cz with pagination
* Send transactional email
* Custom metrics in CloudWatch

### 0.2.0

* Reduce the package size with the introduction of Webpack
* Unit tests

### 0.3.0

* GitLab CI
* SSM Parameters Store

### 0.4.0

* Integration tests
* Use Twilio to send message to the customers
