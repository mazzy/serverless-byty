const Postmark = require("postmark");
const SendEmailError = require("./errors");

const client = new Postmark.Client(process.env.POSTMARK_API_KEY);

module.exports = body => {
  const options = {
    From: process.env.POSTMARK_SENDER,
    To: process.env.POSTMARK_TO,
    TemplateId: process.env.POSTMARK_TEMPLATE_ID,
    TemplateModel: body
  };

  return new Promise((resolve, reject) => {
    client.sendEmailWithTemplate(options, err => {
      if (err) {
        reject(
          new SendEmailError("Unable to send via postmark: " + err.message)
        );
        return;
      }

      resolve();
    });
  });
};
