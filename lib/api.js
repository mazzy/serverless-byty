const AWSXRay = require("aws-xray-sdk");
const https = AWSXRay.captureHTTPs(require("https"));
const qs = require("querystring");
const ResponseAPIError = require("./errors");

const request = (path, query) =>
  new Promise((resolve, reject) => {
    const options = {
      hostname: "www.sreality.cz",
      port: 443,
      path: `${path}?${qs.stringify(query)}`,
      method: "GET"
    };

    const req = https.request(options, res => {
      res.setEncoding("utf8");

      let rawData = "";

      res.on("data", chunk => {
        rawData += chunk;
      });

      res.on("end", () => {
        try {
          resolve(JSON.parse(rawData));
        } catch (err) {
          reject(err);
        }
      });
    });

    req.on("error", err => {
      reject(
        new ResponseAPIError("SReality API error: " + JSON.stringify(err))
      );
    });

    req.end();
  });

module.exports.countEstates = () => {
  const path = "/api/cs/v2/estates/count";
  const query = {
    category_main_cb: 1,
    category_sub_cb: "8|10",
    category_type_cb: 1,
    czk_price_summary_order2: "0|9000000",
    locality_country_id: 112,
    locality_region_id: 10,
    usable_area: "120|200"
  };

  return request(path, query).then(data => data.result_size);
};

module.exports.getEstates = page => {
  const path = "/api/cs/v2/estates";
  const query = {
    category_main_cb: 1,
    category_sub_cb: "8|10",
    category_type_cb: 1,
    czk_price_summary_order2: "0|9000000",
    locality_region_id: 10,
    tms: Date.now(),
    usable_area: "120|200",
    per_page: 20,
    page
  };

  return request(path, query).then(data => data._embedded.estates); // eslint-disable-line no-underscore-dangle
};
