const awsXRay = require("aws-xray-sdk");
const AWS = awsXRay.captureAWS(require("aws-sdk"));

const cloudwatch = new AWS.CloudWatch();

// define metrics
module.exports.SEARCH_FETCHED_ITEMS = "SearchFetchedItems";
module.exports.SEARCH_NEW_ITEMS = "SearchNewItems";

module.exports.putMetricData = (funcName, data) => {
  const params = {
    MetricData: data.map(d => ({
      MetricName: d.name,
      Dimensions: [
        {
          Name: "FunctionName",
          Value: funcName
        }
      ],
      Unit: "Count",
      Timestamp: new Date().toISOString(),
      Value: d.value
    })),
    Namespace: "AWS/Lambda"
  };

  return cloudwatch.putMetricData(params).promise();
};
