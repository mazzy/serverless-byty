class APIResponseError extends Error {
  constructor(message) {
    super(message);
    this.name = "APIResponseError [ERR_API_RESPONSE]";
    this.code = "ERR_RESPONSE_API";

    Error.captureStackTrace(this, APIResponseError);
  }
}

module.exports = APIResponseError;
