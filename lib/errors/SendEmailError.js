class SendEmailError extends Error {
  constructor(message) {
    super(message);
    this.name = "SendEmailError [ERR_SEND_EMAIL]";
    this.code = "ERR_SEND_EMAIL";

    Error.captureStackTrace(this, SendEmailError);
  }
}

module.exports = SendEmailError;
