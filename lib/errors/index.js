const ResponseAPIError = require("./ResponseAPIError");
const SendEmailError = require("./SendEmailError");

module.exports = {
  ResponseAPIError,
  SendEmailError
};
