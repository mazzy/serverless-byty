class HashTable {
  constructor() {
    this.size = 0;
    this.data = {};
  }

  clear() {
    this.data = {};
    this.size = 0;
    return this.data;
  }

  contains(key) {
    if (Object.prototype.hasOwnProperty.call(this.data, key)) {
      return true;
    }
    return false;
  }

  remove(key) {
    if (Object.prototype.hasOwnProperty.call(this.data, key)) {
      this.size -= 1;
      delete this.data[key];
      return this.data;
    }
    return this.data;
  }

  size() {
    return this.size;
  }

  enumarate() {
    Object.entries(this.data).forEach(key => {
      console.log({ [key]: this.data[key] }); // eslint-disable-line no-console
    });
  }

  isEmpty() {
    return !(this.size > 0);
  }

  put(key, value) {
    if (Object.prototype.hasOwnProperty.call(this.data, key)) {
      throw new Error("Hash Table cannot contain duplicate value");
    }

    this.size += 1;
    this.data[key] = value;
  }
}

module.exports = HashTable;
