// this is the map from SReality.cz
const mapSubCategoryCb = category =>
  ({
    8: "4+kk",
    10: "5+kk"
  }[category]);

module.exports.mapSubCategoryCb = mapSubCategoryCb;

const buildUrl = e => {
  const type = mapSubCategoryCb(e.seo.category_sub_cb);
  const name = e.seo.locality;

  return [
    "https://www.sreality.cz/detail/prodej/byt/",
    type,
    "/",
    name,
    "/",
    e.hash_id,
    "#img=0&fullscreen=false"
  ].join("");
};

module.exports.buildUrl = buildUrl;

module.exports.mapEstateProps = e => ({
  id: e.hash_id,
  name: e.name,
  locality: e.locality,
  price_czk: e.price_czk.value_raw,
  type: mapSubCategoryCb(e.seo.category_sub_cb),
  link: buildUrl(e),
  images: e._links.images.map(img => img.href), // eslint-disable-line no-underscore-dangle
  gps: {
    lat: e.gps.lat,
    lon: e.gps.lon
  }
});

// remove the estates which the price is not defined
// these are properties too expensive
module.exports.filterLuxuryEstate = e => e.price_czk !== 1;

module.exports.mapItemSchema = item => ({
  id: item.id,
  name: item.name,
  locality: item.locality,
  price_czk: item.price_czk,
  type: item.type,
  link: item.link,
  lat: item.gps.lat,
  lon: item.gps.lon,
  images: item.images || [],
  created_at: Date.now(),
  updated_at: Date.now()
});
